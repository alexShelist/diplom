import React from 'react'
import PropTypes from 'prop-types'
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import { connect } from 'react-redux';
import { logout } from '../../store/session';
import './PageLayout.scss'

const mapActionCreators = {
    logout
};

const mapStateToProps = (state) => ({
    session: state.session
});

export class PageLayout extends React.Component {
    static propTypes = {
        children    : PropTypes.element.isRequired,
        session     : PropTypes.object.isRequired,
        logout      : PropTypes.func.isRequired
    };

    static contextTypes = {
        router: PropTypes.object
    };

    logout = () => {
        this.props.logout();
        localStorage.removeItem('jwtToken');
    };

    render () {
        const { children } = this.props;

        return (
            <div className='container-fluid'>
                <Header
                    logout={this.logout}
                    session={this.props.session} />
                <div className='core-layout__viewport'>
                    {children}
                </div>
                <Footer/>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapActionCreators)(PageLayout)

import Projects from './components/Projects'

// Sync route definition

export default (store) => ({
    path : 'projects',
    getComponent (nextState, cb) {
        require.ensure([], (require) => {


            /*  Return getComponent   */
            cb(null, Projects)

            /* Webpack named bundle   */
        }, 'projects')
    }
})


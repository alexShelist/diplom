import React from 'react'
import './Projects.scss'
import {browserHistory} from 'react-router'

export const Projects = () => (
    <div className="projects">

        <div className="container text-center">
            <h1 className="text-center mb-5 page__title"><i className="fas fa-share icon__back" onClick={() => {
                browserHistory.push('/join')
            }}/> Наши Проекты </h1>
        </div>
        <div className="container-fluid text-center">
            <div className="row justify-content-around" style={{'margin-top': '20vh'}}>
                <button className="project-btn project-btn__cyber" onClick={() => {
                    window.location.href = 'http://vk.com/cybgarden8'
                }}/>
                <button className="project-btn project-btn__museum" onClick={() => {
                    window.location.href = 'http://vk.com/touch.museum'
                }}/>
                <button className="project-btn project-btn__geek" onClick={() => {
                    window.location.href = 'http://vk.com/geek.school'
                }}/>
            </div>

        </div>

        <button type="button" onClick={() => {
            browserHistory.push('/join')
        }} className="btn btn-outline-primary button__projects">Назад
        </button>

    </div>

);

export default Projects

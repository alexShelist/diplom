import React from 'react'
import './Programs.scss'
import { browserHistory } from 'react-router'

export const Projects = () => (
        <div className="projects">

            <div className="container text-center">
                <h1 className="text-center mb-5 page__title " >  <i className="fas fa-share icon__back" onClick={() => {browserHistory.push('/join')}}/> Наши Программы </h1>
            </div>
            <div className="container-fluid text-center">
                <div className="row justify-content-around" style={{'margin-top': '20vh'}}>
                    <button className="project-btn project-btn__enactus" onClick={() => {window.location.href = 'http://enactus.org/worldcup/'}} />
                    <button className="project-btn project-btn__preactum" onClick={() => {window.location.href = 'http://preactum.ru/'}}/>
                </div>

            </div>

        </div>

);

export default Projects

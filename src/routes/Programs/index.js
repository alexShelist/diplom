import Programs from './components/Programs'

// Sync route definition

export default (store) => ({
    path : 'programs',
    getComponent (nextState, cb) {
        require.ensure([], (require) => {


            /*  Return getComponent   */
            cb(null, Programs)

            /* Webpack named bundle   */
        }, 'programs')
    }
})


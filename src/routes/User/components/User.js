import React from 'react'
import PropTypes from 'prop-types';
import News from 'components/News';
import User from 'components/User';
import {Col, Row} from 'react-bootstrap';

class UserRoute extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            id: 0
        }
    }

    static propTypes = {
        news: PropTypes.object.isRequired,
        updateUserAsync: PropTypes.func.isRequired,
        getNews: PropTypes.func.isRequired,
        getUserById: PropTypes.func.isRequired
    };
    componentWillMount () {
        this.setState({
            id: this.props.routeParams.id
        });
        this.props.getNews(this.props.routeParams.id);
        this.props.getUserById(this.props.routeParams.id);
    }

    updateUser = (userObj) => {
        this.props.updateUserAsync(userObj);
    };


    render () {
        const myPage = (Number(this.props.routeParams.id) === this.props.session.user.id);
        if (this.props.routeParams.id !== this.state.id) {
            this.props.getNews(this.props.routeParams.id);
            this.props.getUserById(this.props.routeParams.id);
        }
        return (
            <div>
                <h2>News by {this.props.user.user.surname} {this.props.user.user.name}</h2>
                <Row className="show-grid">
                    <Col xs={12} md={8}>
                        <News newsItems={this.props.news.newsItems} isLoggedIn={myPage} />
                    </Col>
                    <Col xs={6} md={4} className="news__user" >
                        <User updateUserAsync={this.updateUser} user={this.props.user.user} isLoggedIn={myPage}/>
                    </Col>
                </Row>
            </div>
    )
    }
}

export default UserRoute
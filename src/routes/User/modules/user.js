import {getUser} from "../../../utils/api";

export const GET_USER_SUCCESS = 'GET_USER_SUCCESS';
export const GET_USER_FAIL = 'GET_USER_FAIL';



export function getUserSuccess (value) {
  return {
    type    : GET_USER_SUCCESS,
    payload : value
  }
}

export function getUserFail () {
    return {
        type    : GET_USER_FAIL,
    }
}

export const getUserById = (id) => {
    return async (dispatch, getState) => {

        const user = await getUser(id);
        if (user) {
          dispatch(getUserSuccess(user));
        } else {
          dispatch(getUserFail())
        }
    }
};
// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
      [GET_USER_SUCCESS]: (state, action) => {
        return {
            ...state,
            user: action.payload
        }
      },
    [GET_USER_FAIL]: (state, action) => {
        return {
            ...state,
            user: {}
        }
    },
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
    user: {}
};

export default function dashboardReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state
}

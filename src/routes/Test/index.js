import Test from './components/Test'

// Sync route definition

export default (store) => ({
    path : 'test',
    getComponent (nextState, cb) {
        require.ensure([], (require) => {


            /*  Return getComponent   */
            cb(null, Test)

            /* Webpack named bundle   */
        }, 'test')
    }
})


import React from 'react'
import Db from '../../../../Db'


export default class UserForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedOption: '',
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const formData = {};
        for (const field in this.refs) {
            formData[field] = this.refs[field].value;
        }
        Db.addResult({...formData, ...{answers: this.props.answers}})
        this.form.reset()
        this.props.answers = []
    }

    render() {
        return(
            <div className="container">
                <div className="row">
                    <div className="alert alert-success" role="alert">
                        <h4 className="alert-heading">Отличная работа!</h4>
                        <p> Тестирование окончено. Осталось совсем немного. Введите данные о себе, чтоб мы могли с вами связаться. Спасибо!</p>
                    </div>
                    <form className="w-75" ref={(form) => this.form = form} onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <label htmlFor="name">Укажите выши полные данные:</label>
                            <input ref="name" className="form-control" required={true} id="name" placeholder="Введте ФИО"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="degree">Укажите институт, в котором вы обучаетесь:</label>
                            <input ref="degree" className="form-control" required={true} id="degree" placeholder="Введите институт"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="group">Укажите группу, в которой вы обучаетесь:</label>
                            <input ref="group" className="form-control" required={true} id="group" placeholder="Введите группу"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="phone">Укажите телефон для связи:</label>
                            <input ref="phone" type="phone" className="form-control" id="phone" placeholder="Введите телефон">
                            </input>
                        </div>
                        <div className="form-group">
                            <label htmlFor="email">Укажите почту для связи:</label>
                            <input ref="email" type="email" className="form-control" required={true} id="email"  placeholder="Введите"/>
                        </div>
                        <button type="submit" className="btn btn-primary">Закончить и отправить результаты</button>
                    </form>
                </div>
            </div>
        )
    }
}


import React from 'react'
import './Test.scss'
import TestItem from './TestItem'
import UserForm from './UserForm'
import Modal from 'react-bootstrap4-modal'
import Countdown from 'react-countdown-now';

import {browserHistory} from 'react-router'


import question1 from './TestItem/assets/question1.jpg'
import question2 from './TestItem/assets/question2.jpg'
import question3 from './TestItem/assets/question3.jpg'
import question4 from './TestItem/assets/question4.jpg'
import question5 from './TestItem/assets/question5.jpg'
import question10 from './TestItem/assets/question10.jpg'
import question11 from './TestItem/assets/question11.jpg'
import question12 from './TestItem/assets/question12.jpg'
import question13 from './TestItem/assets/question13.jpg'
import question14 from './TestItem/assets/question14.jpg'

const data = [
    [{
        id: 1,
        question: 'Каково соотношение студентов, подавших заявление в Финансовую Академию при Правительстве в 2017 году относительно 2016 года?',
        type: 1,
        image: question1,
        answers: [
            {
                id: 1,
                text: '0.91:1',
                isRight: false,
            },
            {
                id: 2,
                text: '0.93:1',
                isRight: true,
            },
            {
                id: 3,
                text: '1.08:1',
                isRight: false,
            },
            {
                id: 4,
                text: '1.16:1',
                isRight: false,
            },

        ]
    },
        {
            id: 2,
            question: 'Каково процентное значение снижения общего количества проданных домой ГК «Дружба» и «Главстрой» с Мая по Июнь?',
            image: question2,
            type: 1,
            answers: [
                {
                    id: 5,
                    text: '18,18%',
                    isRight: false,
                },
                {
                    id: 6,
                    text: '13,04%',
                    isRight: true,
                },
                {
                    id: 7,
                    text: '13,26%',
                    isRight: false,
                },
                {
                    id: 8,
                    text: '8,33%',
                    isRight: false,
                },

            ]
        },
        {
            id: 3,
            question: 'Предположим, что половина угля, добытого в обеих шахтах в марте, была продана в апреле, и по четверти продано в мае и июне по ценам этих месяцев. Какой доход был получен в ходе продажи?',
            image: question3,
            type: 1,
            answers: [
                {
                    id: 9,
                    text: '$8 216 250',
                    isRight: false,
                },
                {
                    id: 10,
                    text: '$8 163 750',
                    isRight: false,
                },
                {
                    id: 11,
                    text: '$8 347 500',
                    isRight: true,
                },
                {
                    id: 12,
                    text: '$8 478 750',
                    isRight: false,
                },

            ]
        },
        {
            id: 4,
            question: 'Александр купил 145 акций в начале 2-го года и продал их в начале 5-го года. Какую прибыль ему принесла сделка, учитывая все полученные дивиденды?',
            image: question4,
            type: 1,
            answers: [
                {
                    id: 13,
                    text: '$24650',
                    isRight: false,
                },
                {
                    id: 14,
                    text: '$67860',
                    isRight: false,
                },
                {
                    id: 15,
                    text: '$18125',
                    isRight: false,
                },
                {
                    id: 16,
                    text: '$19865',
                    isRight: true,
                },

            ]
        },
        {
            id: 5,
            question: 'Предположим, что на Рейс А билет стоит 2 доллара, билет на Рейс В стоит больше на 12%, а на Рейс С – на 6% меньше, чем рейс В. В таком случае, какой доход получат транспортные компании с продажи билетов между 8:00 – 9:00 часами утра? (округлить до целого)',
            image: question5,
            type: 1,
            answers: [
                {
                    id: 17,
                    text: '$15864',
                    isRight: false,
                },
                {
                    id: 18,
                    text: '$5299',
                    isRight: true,
                },
                {
                    id: 19,
                    text: '$4767',
                    isRight: false,
                },
                {
                    id: 20,
                    text: '$4366',
                    isRight: false,
                },

            ]
        },],
    [{
        id: 6,
        question: 'Более низкие брокерские комиссионные, в сравнении с комиссией аукционных домов, делают торговлю акциями выгоднее, чем торговля антиквариатом.',
        text: 'Аукционные дома, занимающиеся продажей предметов искусства, были загружены работой в последний год, как никогда ранее, так как инвесторы решили разнообразить свои финансовые вклады. Традиционные вложения в собственность, акции и облигационные инвестиции стали очень нестабильными в последние несколько лет, в то время как скупка антиквариата сильно укрепилась, обгоняя многие показатели индексов финансовых рынков. Аукционеры, в любом случае, получают 5% от стоимости любого предмета, который они продают. Это снижает выгоду торговли антиквариатом, так как комиссионные брокеру гораздо выше, чем затраты на торговлю акциями, которые обычно составляют 1% на транзакцию.\n' +
        '<br/>' +
        'Пока антиквариат остается привлекательной альтернативой для капиталовложений, некоторые категории антикварных предметов упали в цене за последние годы. Большинство экспертов предостерегают коллекционеров внимательно изучать категория антиквариата, в который они хотят вложиться, перед покупкой. Их главным советом является то, что не стоит инвестировать ради самих инвестиций, а наоборот – приобретать предмет, который нравится, и который вы действительно цените',
        type: 2,
        answers: [
            {
                id: 21,
                text: 'Верно',
                isRight: true,
            },
            {
                id: 22,
                text: 'Неверно',
                isRight: false,
            },
            {
                id: 23,
                text: 'Невозможно дать определенный ответ',
                isRight: false,
            },
        ]
    },
        {
            id: 7,
            question: 'Одним из самых больших преимуществ антиквариата заключается в том, что он предполагает больший доход в будущем, нежели традиционные формы инвестиций',
            text: 'Аукционные дома, занимающиеся продажей предметов искусства, были загружены работой в последний год, как никогда ранее, так как инвесторы решили разнообразить свои финансовые вклады. Традиционные вложения в собственность, акции и облигационные инвестиции стали очень нестабильными в последние несколько лет, в то время как скупка антиквариата сильно укрепилась, обгоняя многие показатели индексов финансовых рынков. Аукционеры, в любом случае, получают 5% от стоимости любого предмета, который они продают. Это снижает выгоду торговли антиквариатом, так как комиссионные брокеру гораздо выше, чем затраты на торговлю акциями, которые обычно составляют 1% на транзакцию.\n' +
            '<br/>' +
            'Пока антиквариат остается привлекательной альтернативой для капиталовложений, некоторые категории антикварных предметов упали в цене за последние годы. Большинство экспертов предостерегают коллекционеров внимательно изучать категория антиквариата, в который они хотят вложиться, перед покупкой. Их главным советом является то, что не стоит инвестировать ради самих инвестиций, а наоборот – приобретать предмет, который нравится, и который вы действительно цените',
            type: 2,
            answers: [
                {
                    id: 24,
                    text: 'Верно',
                    isRight: false,
                },
                {
                    id: 25,
                    text: 'Неверно',
                    isRight: false,
                },
                {
                    id: 26,
                    text: 'Невозможно дать определенный ответ',
                    isRight: true,
                },
            ]
        },
        {
            id: 8,
            question: 'Не покупайте предметы искусства только ради инвестиций – таков совет большинства экспертов.',
            text: 'Аукционные дома, занимающиеся продажей предметов искусства, были загружены работой в последний год, как никогда ранее, так как инвесторы решили разнообразить свои финансовые вклады. Традиционные вложения в собственность, акции и облигационные инвестиции стали очень нестабильными в последние несколько лет, в то время как скупка антиквариата сильно укрепилась, обгоняя многие показатели индексов финансовых рынков. Аукционеры, в любом случае, получают 5% от стоимости любого предмета, который они продают. Это снижает выгоду торговли антиквариатом, так как комиссионные брокеру гораздо выше, чем затраты на торговлю акциями, которые обычно составляют 1% на транзакцию.\n' +
            '<br/>' +
            'Пока антиквариат остается привлекательной альтернативой для капиталовложений, некоторые категории антикварных предметов упали в цене за последние годы. Большинство экспертов предостерегают коллекционеров внимательно изучать категория антиквариата, в который они хотят вложиться, перед покупкой. Их главным советом является то, что не стоит инвестировать ради самих инвестиций, а наоборот – приобретать предмет, который нравится, и который вы действительно цените',
            type: 2,
            answers: [
                {
                    id: 27,
                    text: 'Верно',
                    isRight: true,
                },
                {
                    id: 28,
                    text: 'Неверно',
                    isRight: false,
                },
                {
                    id: 29,
                    text: 'Невозможно дать определенный ответ',
                    isRight: false,
                },
            ]
        },
        {
            id: 9,
            question: 'Сегодня больше предметов искусства продается через аукционные дома, нежели в прошлом.',
            text: 'Аукционные дома, занимающиеся продажей предметов искусства, были загружены работой в последний год, как никогда ранее, так как инвесторы решили разнообразить свои финансовые вклады. Традиционные вложения в собственность, акции и облигационные инвестиции стали очень нестабильными в последние несколько лет, в то время как скупка антиквариата сильно укрепилась, обгоняя многие показатели индексов финансовых рынков. Аукционеры, в любом случае, получают 5% от стоимости любого предмета, который они продают. Это снижает выгоду торговли антиквариатом, так как комиссионные брокеру гораздо выше, чем затраты на торговлю акциями, которые обычно составляют 1% на транзакцию.\n' +
            '<br/>' +
            'Пока антиквариат остается привлекательной альтернативой для капиталовложений, некоторые категории антикварных предметов упали в цене за последние годы. Большинство экспертов предостерегают коллекционеров внимательно изучать категория антиквариата, в который они хотят вложиться, перед покупкой. Их главным советом является то, что не стоит инвестировать ради самих инвестиций, а наоборот – приобретать предмет, который нравится, и который вы действительно цените',
            type: 2,
            answers: [
                {
                    id: 30,
                    text: 'Верно',
                    isRight: true,
                },
                {
                    id: 31,
                    text: 'Неверно',
                    isRight: false,
                },
                {
                    id: 32,
                    text: 'Невозможно дать определенный ответ',
                    isRight: false,
                },
            ]
        },
        {
            id: 10,
            question: 'Антиквариат – это альтернативный и конкурентоспособный способ инвестиций.',
            text: 'Аукционные дома, занимающиеся продажей предметов искусства, были загружены работой в последний год, как никогда ранее, так как инвесторы решили разнообразить свои финансовые вклады. Традиционные вложения в собственность, акции и облигационные инвестиции стали очень нестабильными в последние несколько лет, в то время как скупка антиквариата сильно укрепилась, обгоняя многие показатели индексов финансовых рынков. Аукционеры, в любом случае, получают 5% от стоимости любого предмета, который они продают. Это снижает выгоду торговли антиквариатом, так как комиссионные брокеру гораздо выше, чем затраты на торговлю акциями, которые обычно составляют 1% на транзакцию.\n' +
            '<br/>' +
            'Пока антиквариат остается привлекательной альтернативой для капиталовложений, некоторые категории антикварных предметов упали в цене за последние годы. Большинство экспертов предостерегают коллекционеров внимательно изучать категория антиквариата, в который они хотят вложиться, перед покупкой. Их главным советом является то, что не стоит инвестировать ради самих инвестиций, а наоборот – приобретать предмет, который нравится, и который вы действительно цените',
            type: 2,
            answers: [
                {
                    id: 33,
                    text: 'Верно',
                    isRight: false,
                },
                {
                    id: 34,
                    text: 'Неверно',
                    isRight: false,
                },
                {
                    id: 35,
                    text: 'Невозможно дать определенный ответ',
                    isRight: true,
                },
            ]
        },],
    [{
        id: 11,
        question: 'Фигуры меняются в определенной последовательности слева направо. Определите, какое изображение из обозначенных буквами от А до Д должно находиться на пустующем месте.',
        type: 1,
        image: question10,
        answers: [
            {
                id: 36,
                text: 'А',
                isRight: false,
            },
            {
                id: 37,
                text: 'Б',
                isRight: true,
            },
            {
                id: 38,
                text: 'В',
                isRight: false,
            },
            {
                id: 39,
                text: 'Г',
                isRight: false,
            },
            {
                id: 40,
                text: 'Д',
                isRight: false,
            },
        ]
    },
        {
            id: 12,
            question: 'Фигуры меняются в определенной последовательности слева направо. Определите, какое изображение из обозначенных буквами от А до Д должно находиться на пустующем месте.',
            image: question11,
            type: 1,
            answers: [
                {
                    id: 41,
                    text: 'А',
                    isRight: false,
                },
                {
                    id: 42,
                    text: 'Б',
                    isRight: true,
                },
                {
                    id: 43,
                    text: 'В',
                    isRight: false,
                },
                {
                    id: 44,
                    text: 'Г',
                    isRight: false,
                },
                {
                    id: 45,
                    text: 'Д',
                    isRight: false,
                },
            ]
        },
        {
            id: 13,
            question: 'Фигуры меняются в определенной последовательности слева направо. Определите, какое изображение из обозначенных буквами от А до Д должно находиться на пустующем месте.',
            image: question12,
            type: 1,
            answers: [
                {
                    id: 46,
                    text: 'А',
                    isRight: false,
                },
                {
                    id: 47,
                    text: 'Б',
                    isRight: false,
                },
                {
                    id: 48,
                    text: 'В',
                    isRight: true,
                },
                {
                    id: 49,
                    text: 'Г',
                    isRight: false,
                },
                {
                    id: 50,
                    text: 'Д',
                    isRight: false,
                },
            ]
        },
        {
            id: 14,
            question: 'Фигуры меняются в определенной последовательности слева направо. Определите, какое изображение из обозначенных буквами от А до Д должно находиться на пустующем месте',
            image: question13,
            type: 1,
            answers: [
                {
                    id: 51,
                    text: 'А',
                    isRight: false,
                },
                {
                    id: 52,
                    text: 'Б',
                    isRight: true,
                },
                {
                    id: 53,
                    text: 'В',
                    isRight: false,
                },
                {
                    id: 54,
                    text: 'Г',
                    isRight: false,
                },
                {
                    id: 55,
                    text: 'Д',
                    isRight: false,
                },
            ]
        },
        {
            id: 15,
            question: 'Фигуры меняются в определенной последовательности слева направо. Определите, какое изображение из обозначенных буквами от А до Д должно находиться на пустующем месте.',
            image: question14,
            type: 1,
            answers: [
                {
                    id: 56,
                    text: 'А',
                    isRight: false,
                },
                {
                    id: 57,
                    text: 'Б',
                    isRight: false,
                },
                {
                    id: 58,
                    text: 'В',
                    isRight: false,
                },
                {
                    id: 59,
                    text: 'Г',
                    isRight: true,
                },
                {
                    id: 60,
                    text: 'Д',
                    isRight: false,
                },
            ]
        },],
]

let answers = {}
export default class Test extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            arrQuestons: data[0],
            testStart: false,
            allow: true,
            testEnd: false,
            pointer: 0,
            date: 0,
        }
    }

    onPanic = () => {
        browserHistory.goBack()

    }

    onFirePhasers = () => {
        this.setState({
            testStart: true,
            date: Date.now() + 300000,
        })
    }

    setAnswer = (id, answer) => {
        for (let i = 0; i < this.state.arrQuestons.length; i++) {
            if (this.state.arrQuestons[i].id === id) {
                this.state.arrQuestons[i].answers.map((item) => {
                    if (item.text === answer) {
                        answers[id] = item.isRight
                        return
                    }
                })
            }
        }
    }

    endTest = (timeOut) => {
        if (timeOut !== true && Object.keys(answers).length < (Number(this.state.pointer) + 1) * 5) {
            this.setState({
                allow: false,
            })
            return
        }
        if (this.state.pointer !== data.length - 1) {
            let pointer = this.state.pointer + 1
            this.setState({
                pointer: pointer,
                arrQuestons: data[pointer],
                date: Date.now() + 300000,
            })
        } else {
            console.log(answers)
            this.setState({
                testEnd: true,
            })
        }
    }

    onConfirm = () => {
        if (this.state.pointer !== data.length - 1) {
            let pointer = this.state.pointer + 1
            this.setState({
                pointer: pointer,
                arrQuestons: data[pointer],
                date: Date.now() + 300000,
                allow: true,
            })
        } else {
            this.setState({
                testEnd: true,
                allow: true,
            })
        }
    }

    back = () => {
        this.setState({
            allow: true,
        })
    }

    renderer = ({hours, minutes, seconds, completed}) => {
        if (completed) {
            this.endTest(true)
        } else {
            return <span>{minutes}:{seconds}</span>;
        }
    };

    render() {
        if (this.state.testEnd) {
            return (
                <UserForm answers={answers} />
            )
        }
        if (this.state.testStart) {
            return (
                <div className="container">
                    <div className="myAlert-top alert alert-warning">
                        <strong><Countdown renderer={this.renderer} date={this.state.date}/></strong>
                    </div>
                    {this.state.arrQuestons.map((item) => (
                        <TestItem data={item} key={item.id} setAnswer={this.setAnswer}/>
                    ))}
                    <button type="button" className="btn btn-primary btn-lg btn-block mb-3" onClick={this.endTest}>
                        {this.state.pointer === data.length - 1 ? 'Закончить тестирование' : 'Перейти к следующему блоку'}
                    </button>
                    <Modal visible={!this.state.allow && !this.state.testEnd}>
                        <div className="modal-header">
                            <h5 className="modal-title">Внимание!</h5>
                        </div>
                        <div className="modal-body">
                            <p>У вас остались вопросы без ответа, вы уверены что хотите продолжить?</p>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary" onClick={this.back}>
                                Назад
                            </button>
                            <button type="button" className="btn btn-danger" onClick={this.onConfirm}>
                                Продолжить
                            </button>
                        </div>
                    </Modal>
                </div>
            )
        } else {
            return (
                <Modal visible={!this.state.testStart}>
                    <div className="modal-header">
                        <h5 className="modal-title">Внимание!</h5>
                    </div>
                    <div className="modal-body">
                        <p>Сейчас Вам будет предложен тест состоящий из 5 вопросов. На его выполнение отводится 5 минут.
                            Нажмите кнопку "Продолжить" если готовы.</p>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" onClick={this.onPanic}>
                            Назад
                        </button>
                        <button type="button" className="btn btn-primary" onClick={this.onFirePhasers}>
                            Продолжить
                        </button>
                    </div>
                </Modal>
            )
        }
    }
}


import React from 'react'


export default class TestItem extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedOption: '',
        }
    }

    handleOptionChange = (e) => {
        this.setState({
            selectedOption: e.target.value,
        })
        this.props.setAnswer(this.props.data.id, e.target.value)
    }

    render() {
        const {data} = this.props
        if (data.type === 1) {
        return(
            <div className="card mb-5">
                <div className="card-header">
                    Вопрос {data.id}
                </div>
                <div className="card-body p-4">
                    <h5 className="card-title">{data.question}</h5>
                    <div className="media mt-5">
                        <img className="align-items-start mr-5 question__image" src={data.image}/>
                        <div className="media-body">
                            <h5 className="mt-0">Варианты ответов:</h5>
                            {data.answers.map((item) => (
                                <div key={item.id} className="radio">
                                    <label>
                                        <input type="radio" value={item.text}
                                               className="mr-3"
                                               checked={this.state.selectedOption === item.text}
                                               onChange={this.handleOptionChange} />
                                        {item.text}
                                    </label>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        ) } else {
            return(
                <div className="card mb-5">
                    <div className="card-header">
                        Вопрос {data.id}
                    </div>
                    <div className="card-body p-4 ">
                        <h5 className="card-title">{data.question}</h5>
                        <div className="media mt-5 ">
                            <div className="row align-items-center">
                                <div className="col-8" dangerouslySetInnerHTML={{ __html: data.text }}/>
                                <div className="col-4">
                                    <div className="media-body">
                                        <h5 className="mt-0">Варианты ответов:</h5>
                                        {data.answers.map((item) => (
                                            <div key={item.id} className="radio">
                                                <label>
                                                    <input type="radio" value={item.text}
                                                           className="mr-3"
                                                           checked={this.state.selectedOption === item.text}
                                                           onChange={this.handleOptionChange} />
                                                    {item.text}
                                                </label>
                                            </div>
                                        ))}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
    }
}


// We only need to import the modules necessary for initial render
import CoreLayout from '../layouts/PageLayout/PageLayout'
import Home from './Home'
import JoinRoute from './Join';
import Test from './Test';
import Team from './Team';
import Projects from './Projects';
import Programs from './Programs';
import Admin from './Admin';

/*  Note: Instead of using JSX, we recommend using react-router
    PlainRoute objects to build route definitions.   */

export const createRoutes = (store) => ({
  path        : '/',
  component   : CoreLayout,
  indexRoute  : Home,
  childRoutes : [
      JoinRoute(store),
      Test(store),
      Team(store),
      Projects(store),
      Admin(store),
      Programs(store),
  ]
});

export default createRoutes

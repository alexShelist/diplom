import Join from './components/Join'

// Sync route definition

export default (store) => ({
    path : 'join',
    getComponent (nextState, cb) {
        require.ensure([], (require) => {


            /*  Return getComponent   */
            cb(null, Join)

            /* Webpack named bundle   */
        }, 'join')
    }
})


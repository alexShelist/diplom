import React from 'react'
import './Join.scss'
import sample from '../video/SfedU.mp4'
import poster from '../video/logo.jpg'
import { browserHistory } from 'react-router'

export const HomeView = () => (
    <div className="wrapper">
        <video width="100%" id="bgvid" height="100%" autoPlay muted={true} poster={poster} loop>
            <source src={sample} type="video/mp4"/>
            Тег video не поддерживается вашим браузером.
        </video>
        <div className="main-page">
            <div className="container-fluid">
                <div className="row justify-content-around">
                    <button className="main-page__button" onClick={() => {browserHistory.push('/projects')}}>ПРОЕКТЫ</button>
                    <button className="main-page__button" onClick={() => {browserHistory.push('/programs')}}>О ПРОГРАММАХ</button>
                </div>
                <div className="row justify-content-center">
                    <button className="main-page__button main-page__button_centre" >Команда ИТА ЮФУ</button>
                </div>
                <div className="row justify-content-around">
                    <button className="main-page__button" onClick={() => {browserHistory.push('/team')}}>О НАС</button>
                    <button className="main-page__button" onClick={() => {browserHistory.push('/test')}}>Регистрация</button>
                </div>
            </div>


        </div>

    </div>
);

export default HomeView

import Team from './components/Team'

// Sync route definition

export default (store) => ({
    path : 'team',
    getComponent (nextState, cb) {
        require.ensure([], (require) => {


            /*  Return getComponent   */
            cb(null, Team)

            /* Webpack named bundle   */
        }, 'team')
    }
})


import React from 'react'
import './Team.scss'
import ImageGrid from './ImageGrid'
import { browserHistory } from 'react-router'


export default class Team extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="container">
                <h1 className="text-center page__title mb-4"> <i className="fas fa-share icon__back" onClick={() => {browserHistory.push('/join')}}/> Мы - команда ИТА ЮФУ </h1>
                <ImageGrid />
                <div className="card mt-5 p-4" >
                        <div className="card-body">
                            <h5 className="card-title text-center">Знаете ли вы, что такое Enactus?</h5>
                            <p className="card-text">
                                3 октября 2015 года данный вопрос стал основополагающим, ведь именно этот день был объявлен началом сессии нового набора в команду Enactus Инженерно-технологической академии Южного федерального университета.
                                <br/>
                                А для тех, кто ещё не в курсе, Enactus – это международная программа, объединяющая талантливых и активных студентов. Цель этих молодых людей – создание предпринимательских проектов, решающих социальные, экономические и экологические проблемы во всем мире. Три года назад такое сообщество появилось и в Таганроге, причем за это время ребята успели стать бронзовыми призерами национального кубка Enactus и разработать сильнейший инженерный проект в 2014 – 2015 годах.
                                <br/>
                                Однако впереди следующий сезон. Он открывает широкие просторы для творчества, приносит очередные соревнования, а вместе с тем и новые дарования в состав объединения.
                                <br/>
                                Не важно, сколько тебе лет и чем ты занимаешься: кибернетикой или дизайном. Если ты готов работать и развиваться, то добро пожаловать в команду!</p>
                        </div>
                </div>
            </div>
        )
    }
}

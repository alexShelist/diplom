import React from 'react';
import Gallery from 'react-grid-gallery';
import team1 from '../assets/team1.jpg'
import team2 from '../assets/team2.jpg'
import team3 from '../assets/team3.jpg'
import team4 from '../assets/team4.jpg'


export default class ImageGrid extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            images: [
                {
                    src: team1,
                    thumbnail: team1,
                    thumbnailWidth: 320,
                    thumbnailHeight: 190,
                    caption: 'Команда Enactus ИТА ЮФУ представила проекты на Enactus World Cup',
                },
                {
                    src: team2,
                    thumbnail: team2,
                    thumbnailWidth: 320,
                    thumbnailHeight: 190,
                },
                {
                    src: team3,
                    thumbnail: team3,
                    thumbnailWidth: 320,
                    thumbnailHeight: 190,
                },
                {
                    src: team4,
                    thumbnail: team4,
                    thumbnailWidth: 320,
                    thumbnailHeight: 190,
                },
            ]
        };
    }

    render () {
        return (
            <div style={{
                display: "block",
                minHeight: "1px",
                width: "100%",
                border: "1px solid #ddd",
                overflow: "auto"}}>
                <Gallery
                    images={this.state.images}
                    enableImageSelection={false}/>
            </div>
        );
    }
}



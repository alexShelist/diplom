import React from 'react'
import './Admin.scss'
import Db from '../../../Db'
import ReactTable from "react-table";
import "react-table/react-table.css";
import {browserHistory} from 'react-router'


export default class Admin extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: Db.getResult(),
            isAdmin: sessionStorage.getItem("isAdmin") === 'true',
            error: false,
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const formData = {};
        for (const field in this.refs) {
            formData[field] = this.refs[field].value;
        }
        if (formData.login === 'valeria' && formData.password === '12345') {
            this.setState({isAdmin: true, error: false})
            sessionStorage.setItem('isAdmin', 'true')
        } else {
            this.setState({error: true})
        }
    }

    render() {
        const {data} = this.state;
        const columns = [
            {
                Header: "ФИО",
                accessor: "name",
            },
            {
                Header: "Институт",
                accessor: "degree",
            },
            {
                Header: "Группа",
                accessor: "group",
            },
            {
                Header: "Телефон",
                accessor: "phone"
            },
            {
                Header: "Почта",
                accessor: "email"
            },
            {
                Header: "Результат",
                id: 'result',
                accessor: d => {
                    let sum = 0;
                    for (let key in d.answers) {
                        if (d.answers[key] === true) sum++
                    }
                    return (<div
                        dangerouslySetInnerHTML={{
                            __html: `${sum} из 15 `
                        }}
                    />)
                }
            }
        ];
        return (
            <div>
                <div className="container text-center">
                    <h1 className="text-center mb-5 page__title"><i className="fas fa-share icon__back" onClick={() => {
                        browserHistory.push('/join')
                    }}/> Результаты: </h1>
                </div>
                {(this.state.isAdmin) ? <ReactTable
                    data={data}
                    columns={columns}
                    defaultPageSize={10}
                    className="-striped -highlight"
                    SubComponent={row => {
                        const data = row.original.answers
                        let block1 = 0;
                        let block2 = 0;
                        let block3 = 0;
                        for (let key in data) {
                            if (Number(key) < 6) {
                                if (data[key] === true) block1++
                            } else if (Number(key) > 10) {
                                if (data[key] === true) block3++
                            } else {
                                if (data[key] === true) block2++
                            }
                        }
                        return (
                            <div style={{padding: "20px"}}>
                                <em>
                                    Блок 1: {block1}/5
                                </em>
                                <br/>
                                <em>
                                    Блок 2: {block2}/5
                                </em>
                                <br/>
                                <em>
                                    Блок 3: {block3}/5
                                </em>
                                <br/>
                            </div>
                        );
                    }}
                /> : <div className="container">
                    <em className="mb-4">
                        Для просмотра данной страницы необходимо залогиниться в учетную запись администратора.
                    </em>
                    <form className="w-50 mt-5" ref={(form) => this.form = form} onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <input ref="login" className="form-control" required={true} id="login"
                                   placeholder="Логин"/>
                        </div>
                        <div className="form-group">
                            <input ref="password" type="password" className="form-control" required={true} id="password"
                                   placeholder="Пароль"/>
                        </div>
                        {this.state.error && <div className="alert alert-danger">Неправильный логин или пароль</div>}
                        <button type="submit" className="btn btn-primary">Войти</button>
                    </form>
                </div>}
            </div>
        );
    }
}



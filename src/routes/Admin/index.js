import Admin from './components/Admin'

// Sync route definition

export default (store) => ({
    path : 'admin',
    getComponent (nextState, cb) {
        require.ensure([], (require) => {


            /*  Return getComponent   */
            cb(null, Admin)

            /* Webpack named bundle   */
        }, 'admin')
    }
})


import React from 'react'
import './HomeView.scss'
import sample from '../video/SfedU.mp4'
import poster from '../video/logo.jpg'
import { browserHistory } from 'react-router'

export const HomeView = () => (
  <div className="wrapper">
    <video width="100%" id="bgvid" height="100%" autoPlay muted={true} poster={poster} loop>
      <source src={sample} type="video/mp4"/>
        Тег video не поддерживается вашим браузером.
    </video>
      <div id="info-block">
          <h1>Проектная деятельность в ИТА ЮФУ</h1>

      </div>
      <button className="info-block__button" onClick={() => {browserHistory.push('/join')}}>Присоединиться</button>
  </div>
);

export default HomeView

import React from 'react'
import PropTypes from 'prop-types'
import { Panel, ListGroup, ListGroupItem, Col, Image } from 'react-bootstrap'
import { Link } from 'react-router'

export default class NewsItem extends React.Component {
    static propTypes = {
        news: PropTypes.object.isRequired,
    };

    render () {
        const news = this.props.news;
        const userName = news.user.surname + ' ' + news.user.name;
        return(
            <div>
                <Panel header={<h2>{news.title}</h2>} footer={<Link to={`/user/${news.user.id}`} >{userName}</Link>}>
                    {(news.image) ? <Image className='news__image' height={200} src={news.image} rounded /> : ''}
                    {news.text}
                    <ListGroup fill>
                        <ListGroupItem>Tags: {news.tags.map((tag, index) => (
                            <span key={index} ><Link to={`/news/tag/${tag.text}`} >{tag.text}</Link>   </span>
                        ))}</ListGroupItem>
                    </ListGroup>
                </Panel>
            </div>
        );

    }
}

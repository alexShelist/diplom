import React from 'react'
import PropTypes from 'prop-types'
import NewsItem from './NewsItem'
import { Link  } from 'react-router'
import {Pagination, Panel, Button, FormControl, FormGroup, ControlLabel} from 'react-bootstrap'
import TagsInput from 'react-tagsinput';
import 'react-tagsinput/react-tagsinput.css'

export default class News extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activePage: 1,
            filterByName: '',
            filterByText: '',
            tags: [],
            filterByAuthor: '',
        };
    }

    handleSelect = (eventKey) => {
        this.setState({
            activePage: eventKey
        });
    };

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    };

    resetFilters = ev => {
        ev.preventDefault();
        this.setState({
            filterByName: '',
            filterByText: '',
            tags: []
        });
    };

    tagsChange = (tags) => {
        this.setState({tags: tags})
    };

    filterArray = (item) => {
        let filterByName = true;
        let filterByText = true;
        let filterByTag = true;
        let filterByAuthor = true;

        if (this.state.filterByName.length > 0) {
            if (item.title.search(this.state.filterByName) === -1) {
                filterByName = false;
            }
        }
        if (this.state.filterByText.length > 0) {
            if (item.text.search(this.state.filterByText) === -1) {
                filterByText = false;
            }
        }
        if (this.state.tags.length > 0) {
            let arrTags =  this.state.tags;
            let array3 =  item.tags.filter(function(obj) {return arrTags.indexOf(obj.text) >= 0; });
            (array3.length > 0) ? filterByTag = true : filterByTag = false;
        }
        if (this.state.filterByAuthor.length > 0) {
            const userName = item.user.surname + ' ' + item.user.name;
            if (userName.search(this.state.filterByAuthor) === -1) {
                filterByAuthor = false;
            }
        }
        return filterByName && filterByText && filterByTag && filterByAuthor;
    };

    static propTypes = {
        newsItems: PropTypes.array.isRequired
    };
    render () {
        let filtrationArr = this.props.newsItems.filter(this.filterArray);
        const items = filtrationArr.length;
        const itemPerPage = 3;
        const pages = Math.ceil(items/itemPerPage);
        const start = (this.state.activePage - 1) * itemPerPage;
        const finish = (this.state.activePage * itemPerPage);


        return(

            <div>


                {(this.props.isLoggedIn) ? <Link to='/news/createNews' className='btn btn-primary btn-lg news__btn'>Create new news</Link> : ''}

                <Panel collapsible header="News filters" bsStyle="primary">
                    <form>
                        <FormGroup controlId="filterByName" bsSize="large">
                            <ControlLabel>filter by name</ControlLabel>
                            <FormControl
                                autoFocus
                                value={this.state.filterByName}
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <FormGroup controlId="filterByText" bsSize="large">
                            <ControlLabel>filter by text</ControlLabel>
                            <FormControl
                                value={this.state.filterByText}
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <FormGroup controlId="filterByTag" bsSize="large">
                            <ControlLabel>Tags</ControlLabel>
                            <TagsInput value={this.state.tags}
                                       addOnBlur={true}
                                       onChange={this.tagsChange} />
                        </FormGroup>
                        <FormGroup controlId="filterByAuthor" bsSize="large">
                            <ControlLabel>filter by Author</ControlLabel>
                            <FormControl
                                value={this.state.filterByAuthor}
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <Button
                            block
                            bsSize="large"
                            bsStyle="warning"
                            onClick={this.resetFilters}
                        >
                            Reset filters
                        </Button>
                    </form>
                </Panel>

                {filtrationArr.map((item, i) => (
                    ( i >= start && i < finish) ? <NewsItem news={item} key={i}/>: ''
                ))}

                {(items > itemPerPage) ? <div className='text-center'> <Pagination
                    prev
                    next
                    first
                    last
                    ellipsis
                    boundaryLinks
                    items={pages}
                    maxButtons={5}
                    activePage={this.state.activePage}
                    onSelect={this.handleSelect} /></div> : ''}
            </div>
        );

    }
}

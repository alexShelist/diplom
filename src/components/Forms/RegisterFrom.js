import React, { Component } from "react";
import { Button, FormGroup, FormControl, ControlLabel, ListGroupItem, ListGroup } from "react-bootstrap";

export default class RegisterFrom extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            confirmPassword: "",
            surname: "",
            name: "",
            error: false,
            emailValid: false,
            passwordValid: false,
            confirmPasswordValid: false,
        };
    }

    validateForm() {
        return this.state.email.length > 0
            && this.state.password.length > 0
            && this.state.password === this.state.confirmPassword;
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    };

    handleRegister = ev => {
        ev.preventDefault();
        this.setState({error: true});
        if (this.state.email.trim().length > 0
            && this.state.password.trim().length > 0
            && this.state.password === this.state.confirmPassword) {
            this.props.handleRegister(this.state);
        } else {
            if (!this.state.email.trim().length > 0) {
                this.setState({
                    emailValid: true
                });
            }
            if (!this.state.password.trim().length > 0) {
                this.setState({
                    passwordValid: true
                });
            }
            if (this.state.password !== this.state.confirmPassword) {
                this.setState({
                    confirmPasswordValid: true
                });
            }

        }
    };

    render() {
        return (
            <div className="Login">
                {(this.props.session.loginToken === "invalid" && this.state.error) ? <ListGroup><ListGroupItem bsStyle="danger">Invalid data</ListGroupItem></ListGroup>: ''}
                <form onSubmit={this.handleRegister}>
                    <FormGroup controlId="email" bsSize="large" validationState={(this.state.emailValid) ? 'error': null}>
                        <ControlLabel>Email</ControlLabel>
                        <FormControl
                            autoFocus
                            type="email"
                            value={this.state.email}
                            onChange={this.handleChange}
                        />
                    </FormGroup>
                    <FormGroup controlId="password" bsSize="large" validationState={(this.state.passwordValid) ? 'error': null}>
                        <ControlLabel>Password</ControlLabel>
                        <FormControl
                            value={this.state.password}
                            onChange={this.handleChange}
                            type="password"
                        />
                    </FormGroup>
                    <FormGroup controlId="confirmPassword" bsSize="large" validationState={(this.state.confirmPasswordValid) ? 'error': null}>
                        <ControlLabel>Confirm password</ControlLabel>
                        <FormControl
                            value={this.state.confirmPassword}
                            onChange={this.handleChange}
                            type="password"
                        />
                    </FormGroup>
                    <FormGroup controlId="name" bsSize="large">
                        <ControlLabel>Name</ControlLabel>
                        <FormControl
                            value={this.state.name}
                            onChange={this.handleChange}
                        />
                    </FormGroup>
                    <FormGroup controlId="surname" bsSize="large">
                        <ControlLabel>Surname</ControlLabel>
                        <FormControl
                            value={this.state.surname}
                            onChange={this.handleChange}
                        />
                    </FormGroup>
                    <Button
                        block
                        bsSize="large"
                        disabled={!this.validateForm()}
                        type="submit"
                    >
                        Register
                    </Button>
                </form>
            </div>
        );
    }
}
import React, { Component } from "react";
import { Button, FormGroup, FormControl, ControlLabel, Image, ListGroup, ListGroupItem } from "react-bootstrap";

export default class UpdatePersonalInformation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: (this.props.user.name === null) ? '' : this.props.user.name,
            surname: (this.props.user.surname === null) ? '' : this.props.user.surname,
            description: (this.props.user.description === null) ? '' : this.props.user.description,
            image: [],
            perviousImage: (this.props.user.image === null) ? '' : this.props.user.image,
            loadImage: false,
            nameValid: false,
            surnameValid: false,
            errorValid: false
        };
    }

    validateForm() {
        return this.state.name.length > 0
            && this.state.surname.length > 0;
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    };

    handleUpdate = ev => {
        ev.preventDefault();
        if (this.state.name.trim().length > 0 && this.state.surname.trim().length > 0) {
            this.setState({description: this.state.description.trim()});
            this.props.updateUser(this.state);
            this.setState({errorValid: false});
        } else {
            this.setState({errorValid: true});
            if (!this.state.name.trim().length > 0) {
                this.setState({nameValid: true})
            }
            if (!this.state.surname.trim().length > 0) {
                this.setState({surnameValid: true})
            }
        }
    };

    handleFileUpload = (e) => {
        this.setState({
            image: e.target.files[0],
            loadImage: true
        })
    };

    render() {
        return (
            <div>
                {(this.state.errorValid) ? <ListGroup><ListGroupItem bsStyle="danger">Not valid forms</ListGroupItem></ListGroup>: ''}
                <form onSubmit={this.handleUpdate}>
                    <FormGroup controlId="name" bsSize="large" validationState={(this.state.nameValid) ? 'error': null}>
                        <ControlLabel>Name</ControlLabel>
                        <FormControl
                            autoFocus
                            value={this.state.name}
                            onChange={this.handleChange}
                        />
                    </FormGroup>
                    <FormGroup controlId="surname" bsSize="large" validationState={(this.state.surnameValid) ? 'error': null}>
                        <ControlLabel>Surname</ControlLabel>
                        <FormControl
                            value={this.state.surname}
                            onChange={this.handleChange}
                        />
                    </FormGroup>
                    <FormGroup controlId="description" bsSize="large">
                        <ControlLabel>Description</ControlLabel>
                        <FormControl componentClass="textarea"
                                     value={this.state.description}
                                     onChange={this.handleChange}
                                     placeholder="" />
                    </FormGroup>
                    {(this.props.user.image && !this.state.loadImage) ? <Image height={150} src={this.props.user.image} circle/> : ''}
                    <FormGroup controlId="image" bsSize="large">
                        <ControlLabel>Image</ControlLabel>
                        <FormControl type="file"
                                     accept="image/*"
                                     onChange={this.handleFileUpload}/>
                    </FormGroup>
                    <Button
                        block
                        bsSize="large"
                        disabled={!this.validateForm()}
                        type="submit"
                    >
                        Update
                    </Button>
                </form>
            </div>
        );
    }
}
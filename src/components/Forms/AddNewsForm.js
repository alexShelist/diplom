import React, { Component } from "react";
import { Button, FormGroup, FormControl, ControlLabel, ListGroupItem, ListGroup } from "react-bootstrap";
import TagsInput from 'react-tagsinput';
import 'react-tagsinput/react-tagsinput.css'


export default class AddNewsForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newsName: "",
            formControlsTextarea: "",
            tags: [],
            image: [],
            newsAdded: false,
            newsNameValid: false,
            formControlsTextareaValid:false,
            errorValid: false
        };
        this.tagsChange = this.tagsChange.bind(this);
    }

    validateForm() {
        return this.state.newsName.length > 0
            && this.state.formControlsTextarea.length > 0;
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    };

    tagsChange(tags) {
        this.setState({tags})
    }

    handleCreate = ev => {
        ev.preventDefault();
        if (this.state.newsName.trim().length > 0
            && this.state.formControlsTextarea.trim().length > 0) {
            this.setState({
                newsName: this.state.newsName.trim(),
                formControlsTextarea: this.state.formControlsTextarea.trim()
            });
            this.props.newsAdd(this.state);
            this.setState({
                newsName: "",
                formControlsTextarea: "",
                tags: [],
                newsAdded: true,
                errorValid: false,
                formControlsTextareaValid: false,
                newsNameValid: false
            });
        } else {
            this.setState({errorValid: true});
            if (!this.state.newsName.trim().length > 0) {
                this.setState({newsNameValid: true});
            }
            if (!this.state.formControlsTextarea.trim().length > 0) {
                this.setState({formControlsTextareaValid: true});

            }
        }


    };
    handleFileUpload = (e) => {
        this.setState({image: e.target.files[0]})
    };

    render() {
        return (
            <div className="News">
                {(this.state.newsAdded) ? <ListGroup><ListGroupItem bsStyle="success">Новость добавлена</ListGroupItem></ListGroup>: ''}
                {(this.state.errorValid) ? <ListGroup><ListGroupItem bsStyle="danger">Not valid forms</ListGroupItem></ListGroup>: ''}
                <form onSubmit={this.handleCreate}>
                    <FormGroup controlId="newsName" bsSize="large" validationState={(this.state.newsNameValid) ? 'error': null}>
                        <ControlLabel>News name</ControlLabel>
                        <FormControl
                            autoFocus
                            value={this.state.newsName}
                            onChange={this.handleChange}
                        />
                    </FormGroup>
                    <FormGroup controlId="formControlsTextarea" bsSize="large" validationState={(this.state.formControlsTextareaValid) ? 'error': null}>
                        <ControlLabel>News text</ControlLabel>
                        <FormControl componentClass="textarea"
                                     value={this.state.formControlsTextarea}
                                     onChange={this.handleChange}
                                     placeholder="" />
                    </FormGroup>
                    <FormGroup controlId="tags" bsSize="large">
                        <ControlLabel>Tags</ControlLabel>
                        <TagsInput value={this.state.tags}
                                   addOnBlur={true}
                                   onChange={this.tagsChange} />
                    </FormGroup>
                    <FormGroup controlId="image" bsSize="large">
                        <ControlLabel>Image</ControlLabel>
                        <FormControl type="file"
                                     accept="image/*"
                                     onChange={this.handleFileUpload}/>
                    </FormGroup>
                    <Button
                        block
                        bsSize="large"
                        disabled={!this.validateForm()}
                        type="submit"
                    >
                        Add news
                    </Button>
                </form>
            </div>
        );
    }
}
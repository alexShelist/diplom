import React, { Component } from "react";
import { Button, FormGroup, FormControl, ControlLabel, ListGroup, ListGroupItem } from "react-bootstrap";

export default class LoginForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: "",
            password: "",
            error: false
        };
    }

    validateForm() {
        return this.state.email.length > 0 && this.state.password.length > 0;
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    };

    handleLogin = ev => {
        ev.preventDefault();
        this.setState({error: true});
        this.props.handleLogin(this.state);

    };

    facebookLogin = ev => {
        ev.preventDefault();
        this.props.loginSocial('facebook');
    };

    render() {
        return (
            <div className="Login">
                {(this.props.session.loginToken === "invalid" && this.state.error) ? <ListGroup><ListGroupItem bsStyle="danger">Invalid email or password</ListGroupItem></ListGroup>: ''}
                <form onSubmit={this.handleLogin}>
                    <FormGroup controlId="email" bsSize="large">
                        <ControlLabel>Email</ControlLabel>
                        <FormControl
                            autoFocus
                            type="email"
                            value={this.state.email}
                            onChange={this.handleChange}
                        />
                    </FormGroup>
                    <FormGroup controlId="password" bsSize="large">
                        <ControlLabel>Password</ControlLabel>
                        <FormControl
                            value={this.state.password}
                            onChange={this.handleChange}
                            type="password"
                        />
                    </FormGroup>
                    <Button
                        block
                        bsSize="large"
                        disabled={!this.validateForm()}
                        type="submit"
                    >
                        Login
                    </Button>
                </form>


            </div>
        );
    }
}
import React from 'react'
import PropTypes from 'prop-types'
import {Panel, Button, Image, Col} from 'react-bootstrap';
import UpdatePersonalInformation from '../Forms/UpdatePersonalInformation';

export default class User extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            formVisible: false,
        };
    }
    static propTypes = {
        updateUserAsync: PropTypes.func.isRequired,
        user: PropTypes.object.isRequired
    };

    updateUser = (userObj) => {
        this.props.updateUserAsync(userObj);
        this.setState({formVisible: false});
    };

    render () {
        const user = this.props.user;
        return(
            <div>
                <Panel collapsible defaultExpanded bsStyle="primary" header="Personal information">
                    {(this.props.isLoggedIn && this.state.formVisible) ? <UpdatePersonalInformation updateUser={this.updateUser} user={user}/> : <div>
                        {(this.props.user.image) ? <Image height={150} src={user.image} className="center-block"/> : ''}
                        <h5><strong>Surname:</strong> {user.surname}</h5>
                        <h5><strong>Name:</strong> {user.name}</h5>
                        <h5><strong>Email:</strong> {user.email}</h5>
                        <h5><strong>Description:</strong> {user.description}</h5>
                    </div>}

                    {(this.props.isLoggedIn && !this.state.formVisible) ? <Button
                        onClick={() => {this.setState({formVisible: !this.state.formVisible})}}
                        bsStyle="primary"
                    >
                        Update personal information</Button>: ''}
                </Panel>




            </div>
        );

    }
}

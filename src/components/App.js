import React from 'react'
import { browserHistory, Router } from 'react-router'
import { Provider } from 'react-redux'
import PropTypes from 'prop-types'
import jwt from 'jsonwebtoken';
import setAuthorizationToken from '../utils/setAuthorizationToken';
import { loginSuccess } from '../store/session';

class App extends React.Component {
  static propTypes = {
    store: PropTypes.object.isRequired,
    routes: PropTypes.object.isRequired,
  };

  shouldComponentUpdate () {
    return false
  };


  render () {
    const store = this.props.store;
    if (localStorage.jwtToken) {
        setAuthorizationToken(localStorage.jwtToken);
        store.dispatch(loginSuccess({
            user: jwt.decode(localStorage.jwtToken),
            token: localStorage.jwtToken
        }));
    }
    return (
      <Provider store={store}>
            <div style={{ height: '100%' }}>
              <Router history={browserHistory} children={this.props.routes} />
            </div>
      </Provider>
    )
  }
}

export default App;

import React from 'react'
import './Footer.scss'
import vk from './assets/vk.png'
import email from './assets/email.png'
import { browserHistory } from 'react-router'
export default class Footer extends React.Component {
    constructor(props) {
        super(props);

    }
    logout = () => {
        if (window.location.pathname === '/admin') {
            sessionStorage.clear()
            browserHistory.push('/')
        } else {
            browserHistory.push('/admin')
        }
    }
    render() {
        return (
            <footer className="navbar navbar-default footer">
                <div className="container">
                    <div className="row center ">
                        <div>
                        <img src={vk} className="social-logo" /> : <a href="https://vk.com/valeria_sfedu" className="footer__links">https://vk.com/valeria_sfedu</a>
                        </div>
                    </div>
                    <div className="row center ">
                        <div>
                        <img src={email} className="social-logo" /> : <a href="https://vk.com/valeria_sfedu" className="footer__links">valeria@sfedu.ru</a>
                    </div>
                    </div>
                    <div className="row center ">
                        <div className="footer__links footer__links-admin" onClick={this.logout}>{window.location.pathname === '/admin' ? 'Выход' : 'Администрирование'}</div>
                    </div>
                </div>
            </footer>
        )
    }
}
import React from 'react'
import {IndexLink, Link,} from 'react-router'
// import {NavLink, Link} from 'react-router-dom'
import './Header.scss'
import PropTypes from 'prop-types';
import {browserHistory} from 'react-router'

class Header extends React.Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }

    static propTypes = {
        session: PropTypes.object.isRequired,
        logout: PropTypes.func.isRequired
    };

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
            </nav>
        )
    }
}

export default Header
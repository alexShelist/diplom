import {loginRequest, registerRequest, facebookRequest, updateUserRequest} from '../utils/api'
import setAuthorizationToken from '../utils/setAuthorizationToken';
import {getUserSuccess} from "../routes/User/modules/user";
// ------------------------------------
// Constants
// ------------------------------------
export const SESSION_LOGIN_SUCCESS = 'SESSION_LOGIN_SUCCESS';
export const SESSION_LOGIN_FAIL = 'SESSION_LOGIN_FAIL';
export const SESSION_LOGOUT = 'SESSION_LOGOUT';
const INVALID = "invalid";
// import browserHistory from 'react-router/lib/browserHistory';
import { browserHistory } from 'react-router'
import {getNews} from "./news";
// ------------------------------------
// Actions
// ------------------------------------
export function loginSuccess (value) {
  return {
    type    : SESSION_LOGIN_SUCCESS,
    payload : value
  }
}

export function loginFail (value) {
  return {
    type    : SESSION_LOGIN_FAIL,
    payload : value
  }
}

export function logout () {
    setAuthorizationToken();
    return {
        type    : SESSION_LOGOUT,
    }
}

export const loginAsync = (loginObj) => {
    return async (dispatch, getState) => {

        const {user, token} = await loginRequest(loginObj.email, loginObj.password);
        if (user) {
          dispatch(loginSuccess({user, token}));
            browserHistory.push('/news')
        } else {
          dispatch(loginFail(INVALID))
        }
    }
};

export const registerAsync = (loginObj) => {
    return async (dispatch, getState) => {

        const {user, token} = await registerRequest(loginObj.email, loginObj.password, loginObj.surname, loginObj.name);
        if (user) {
            dispatch(loginSuccess({user, token}));
            browserHistory.push('/news')
        } else {
            dispatch(loginFail(INVALID))
        }
    }
};

export const updateUserAsync = (userObj) => {
    return async (dispatch, getState) => {
        const {user, token} = await updateUserRequest(userObj.surname, userObj.name, userObj.description, userObj.image, userObj.perviousImage);
        if (user) {
            dispatch(loginSuccess({user, token}));
            dispatch(getUserSuccess(user));
            dispatch(getNews(user.id));
        } else {
            dispatch(loginFail(INVALID))
        }
    }
};

export const loginSocial = (social) => {
    return async (dispatch, getState) => {
        const {user, token} = await facebookRequest();
        if (user) {
            dispatch(loginSuccess({user, token}));
            browserHistory.push('/news')
        } else {
            dispatch(loginFail(INVALID))
        }
    }
};
// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
      [SESSION_LOGIN_SUCCESS]: (state, action) => {
        return {
            ...state,
            loginToken: action.payload.token,
            user: action.payload.user,
            isLoggedIn: true
        }
      },
      [SESSION_LOGIN_FAIL]: (state, action) => {
        return {
          ...state,
          loginToken: action.payload
        }
      },
    [SESSION_LOGOUT]: (state, action) => {
        return {
            ...state,
            isLoggedIn: false,
            loginToken: 'none',
            user: {}
        }
    }
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
    isLoggedIn: false,
    loginToken: 'none',
    user: {}
};

export default function dashboardReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state
}

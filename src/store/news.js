// ------------------------------------
// Constants
// ------------------------------------
export const GET_NEWS = 'GET_NEWS';
export const NEWS_ADD_SUCCESS = 'NEWS_ADD_SUCCESS';


import { getAllNews, addNews, getNewsByTag, searchingNews } from '../utils/api'
import { browserHistory } from 'react-router'
// ------------------------------------
// Actions
// ------------------------------------

export const getNews = (id = null) => {
    return async (dispatch, getState) => {
        const payload = await getAllNews(id);
        if (payload) {
            dispatch(getNewsSuccess(payload));
        }
    }
};

export const newsAdd = (news) => {
    return async (dispatch, getState) => {
        const result = await addNews(news);
        if (result) {
            dispatch(newsAddSuccess(result));
            browserHistory.push('/news');
        }
    }
};

export const getNewsByTags = (tag) => {
    return async (dispatch, getState) => {
        const payload = await getNewsByTag(tag);
        if (payload) {
            dispatch(getNewsSuccess(payload));
        }
    }
};

export const searchNews = (search) => {
    return async (dispatch, getState) => {
        const payload = await searchingNews(search);
        if (payload) {
            dispatch(getNewsSuccess(payload));
        }
    }
};



export function newsAddSuccess (newsObj) {
    return {
        type    : NEWS_ADD_SUCCESS,
        payload : newsObj
    }
}

export function getNewsSuccess (value) {
    return {
        type    : GET_NEWS,
        payload : value
    }
}




// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
      [GET_NEWS]    : (state, action) => {
          return {
              ...state,
              newsItems: action.payload
          }
      },
    [NEWS_ADD_SUCCESS]: (state, action) => {
        return {
            ...state
        }
    },
};

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
    newsItems: [ ]
};
export default function newsReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state
}

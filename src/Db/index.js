
class Db {
    addResult = (data) => {
        console.log(typeof localStorage.getItem('result') === 'string')
        let result = (typeof localStorage.getItem('result') === 'string') ? JSON.parse(localStorage.getItem('result')) : []
        result.push(data)
        localStorage.setItem('result', JSON.stringify(result))
    }

    getResult = () => {
        return (typeof localStorage.getItem('result') === 'string') ? JSON.parse(localStorage.getItem('result')) : []
    }
}

let Bd = new Db();

export default Bd
import axios from 'axios';
import setAuthorizationToken from './setAuthorizationToken';

export const loginRequest = async (login, password) => {
  return axios.post('/api/user/login', {
            email: login,
            password: password
        }).then(function (response) {
            setAuthorizationToken(response.data.token);
            localStorage.setItem('jwtToken', response.data.token);
            return response.data;
      })
      .catch(function (error) {
              return "invalid";
      });
};

export const registerRequest = async (login, password, surname, name) => {
    return axios.post('/api/user/', {
        email: login,
        password: password,
        surname: surname,
        name: name
    }).then(function (response) {
        localStorage.setItem('jwtToken', response.data.token);
        setAuthorizationToken(response.data.token);
        return response.data;
    })
        .catch(function (error) {
            console.log(error);
            return "invalid";
        });
};

export const updateUserRequest = async (surname, name, description, image, perviousImage) => {
    let data = new FormData();
    data.append('image', image);
    data.append('surname', surname);
    data.append('name', name);
    data.append('description', description);
    data.append('perviousImage', perviousImage);
    return axios.patch('/api/user/', data)
        .then(function (response) {
        localStorage.removeItem('jwtToken');
        localStorage.setItem('jwtToken', response.data.token);
        setAuthorizationToken(response.data.token);
        return response.data;
    })
        .catch(function (error) {
            console.log(error);
        });
};


export const facebookRequest = async () => {
    return axios.get('/api/user/login/facebook', {
        }).then(function (response) {
            localStorage.setItem('jwtToken', response.data.token);
            return response.data;
        })
            .catch(function (error) {
                console.log(error);
                return "invalid";
        });
};

export const getAllNews = async (id) => {
   if (id === null) {
       return axios.get('/api/news', {}).then(function (response) {
           return response.data;
       })
           .catch(function (error) {
               console.log(error);
               return "invalid";
           });
   } else {
       return axios.get(`/api/news/${id}`, {}).then(function (response) {
           return response.data;
       }).catch(function (error) {
               console.log(error);
               return "invalid";
           });
   }
};

export const getNewsByTag = async (tag) => {
        return axios.get(`/api/news/tag/${tag}`, {}).then(function (response) {
            return response.data;
        })
            .catch(function (error) {
                console.log(error);
                return "invalid";
            });
};

export const searchingNews = async (search) => {
    return axios.get(`/api/news/search/${search}`, {}).
    then(function (response) {
        return response.data;
    })
        .catch(function (error) {
            console.log(error);
            return false;
        });
};

export const addNews = async (news) => {
    let data = new FormData();
    data.append('image', news.image);
    data.append('title', news.newsName);
    data.append('text', news.formControlsTextarea);
    data.append('tags', news.tags);
    return axios.post('/api/news', data).then(function (response) {
        return response.data;
    }).catch(function (error) {
            console.log(error);
            return false;
        });
};

export const getUser = async (id) => {
    return axios.get(`/api/user/${id}`, {})
        .then(function (response) {
        return response.data;
    })
        .catch(function (error) {
        console.log(error);
        return "invalid";
    });
};



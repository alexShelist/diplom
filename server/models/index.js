'use strict';

const fs        = require('fs');
const path      = require('path');
const Sequelize = require('sequelize');
const basename  = path.basename(module.filename);
const env       = process.env.NODE_ENV || 'development';
const config    = require(__dirname + '/../config/config.json')[env];
const db        = {};

const sequelize = new Sequelize(config.database, config.username, config.password, config);

fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf('.') !== 0) && (file !== basename);
  })
  .forEach(function(file) {
    if (file.slice(-3) !== '.js') return;
    const model = sequelize['import'](path.join(__dirname, file));
    db[model.name] = model;
  });

db.sequelize = sequelize;
db.Sequelize = Sequelize;

db.news.belongsTo(db.users);
db.users.hasMany(db.news);

db.news.belongsToMany(db.tags, {through: 'newsTags', foreignKey: 'news_id', timestamps: false});
db.tags.belongsToMany(db.news, {through: 'newsTags', foreignKey: 'tags_id', timestamps: false});


module.exports = db;

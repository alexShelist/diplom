module.exports = function (sequelize, DataTypes) {
    const news = sequelize.define('news', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        title: DataTypes.STRING,
        text: DataTypes.TEXT,
        image: DataTypes.TEXT

    });
    return news;
};
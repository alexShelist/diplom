module.exports = function (sequelize, DataTypes) {
    const tags = sequelize.define('tags', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true},
        text: {
            type: DataTypes.TEXT
        },

    });
    return tags;
};
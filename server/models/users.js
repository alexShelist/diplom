module.exports = function (sequelize, DataTypes) {
    const users = sequelize.define('users', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        email: {
            type: DataTypes.STRING,
            unique: true
        },
        password: {
            type: DataTypes.STRING,
            select: false
        },
        name: DataTypes.STRING,
        surname: DataTypes.STRING,
        description: DataTypes.TEXT,
        facebookId: DataTypes.TEXT,
        twitterId: DataTypes.TEXT,
        image: DataTypes.TEXT
    });
    return users;
};
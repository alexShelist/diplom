module.exports = function (sequelize, DataTypes) {
    const newsTags = sequelize.define('newsTags', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
    });
    return newsTags;
};
const express = require('express');
const router = express.Router();
const controller = require('../controllers/newsController');
const authenticate = require('../middlewares/authenticate');
const multer = require('multer');

const storage = multer.diskStorage({
    destination: 'public/uploads',
    filename(req, file, cb) {
        cb(null, `${new Date()}-${file.originalname}`);
    },
});

const upload = multer({ storage });


router.post('/', authenticate, upload.single('image'), controller.createNews);
router.get('/', controller.getNews);
router.get('/:id', controller.getUserNews);
router.get('/tag/:tag', controller.getNewsByTag);
router.get('/search/:search', controller.searchNews);


module.exports = router;
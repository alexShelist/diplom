const express = require('express');
const router = express.Router();
const controller = require('../controllers/usersController');
const passport = require('passport');
const jwtsecret = require('../config/config.json').jwtsecret;
const jwt = require('jsonwebtoken');
const authenticate = require('../middlewares/authenticate');
const multer = require('multer');

const storage = multer.diskStorage({
    destination: 'public/uploads/avatar',
    filename(req, file, cb) {
        cb(null, `${new Date()}-${file.originalname}`);
    },
});

const upload = multer({ storage });


router.post('/login', passport.authenticate('local-login'),
    (req, res) => {
        const payload = {
            id: req.user.id,
            email: req.user.email,
            name: req.user.name,
            surname: req.user.surname,
            description: req.user.description,
            image: req.user.image
        };
        const token = jwt.sign(payload, jwtsecret);
        res.json({user: payload, token: token});
    });
router.post('/', passport.authenticate('local-register'),
    (req, res) => {
        const payload = {
            id: req.user.id,
            email: req.user.dataValues.email,
            name: req.user.dataValues.name,
            surname: req.user.dataValues.surname,
            description: req.user.dataValues.description,
            image: req.user.dataValues.image
        };
        const token = jwt.sign(payload, jwtsecret);
        res.json({user: payload, token: token});
});

router.get('/logout', controller.logout);

router.get('/login/facebook',
    passport.authenticate('facebook', { scope : 'email' }
    ));


router.get('/login/facebook/callback', passport.authenticate('facebook'), (req, res) => {
    const payload = {
        id: req.user.id,
        email: req.user.dataValues.email,
        name: req.user.dataValues.name,
        surname: req.user.dataValues.surname,
        description: req.user.dataValues.description,
        image: req.user.dataValues.image
    };
    const token = jwt.sign(payload, jwtsecret);
    res.redirect(`/news?jwtToken=${token}`);
});

router.get('/login/twitter', passport.authenticate('twitter', { scope : 'email' }));


router.get('/login/twitter/callback', passport.authenticate('twitter'),
    (req, res) => {
    console.log(req.user);
    const payload = {
        id: req.user.id,
        email: req.user.dataValues.email,
        name: req.user.dataValues.name,
        surname: req.user.dataValues.surname,
        description: req.user.dataValues.description,
        image: req.user.dataValues.image
    };
    const token = jwt.sign(payload, jwtsecret);
    res.redirect(`/news?jwtToken=${token}`);
});



router.get('/:id', controller.getUser);
router.patch('/', authenticate, upload.single('image'), controller.updateUser);

module.exports = router;
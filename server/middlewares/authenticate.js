const jwt = require('jsonwebtoken');
const db = require('../models');
const jwtSecrwt = require('../config/config.json').jwtsecret;

module.exports = function (req, res, next) {

    const authorizationHeader = req.headers['authorization'];
    let token;

    if (authorizationHeader) {
        token = authorizationHeader.split(' ')[1];
    }

    if (token) {
        jwt.verify(token, jwtSecrwt, (err, decoded) => {
            if (err) {
                res.status(401).json({ error: 'Failed to authenticate' });
            } else {
                db.users.find({
                    where: { id: decoded.id },
                    select: [ 'email', 'id', 'username' ]
                }).then(user => {
                    if (!user) {
                        res.status(404).json({ error: 'No such user' });
                    } else {
                        req.currentUser = user;
                        next();
                    }

                });
            }
        });
    } else {
        res.status(403).json({
            error: 'No token provided'
        });
    }
};
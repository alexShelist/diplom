const db = require('../models');

function createNews(req, res) {

    let tags = [];
    if (req.body.tags.length > 0) {
        let tagsArr = req.body.tags.split(',');
        for (let i = 0; i < tagsArr.length; i++) {
            tags.push({text: tagsArr[i].toLowerCase()})
        }
    }
    let filePath = '';
    if (req.file) {
        filePath = '/' + req.file.path.substr(7);
    }
    return db.news.create({
        title: req.body.title,
        text: req.body.text,
        userId: req.currentUser.id,
        tags: tags,
        image: filePath
    }, {
        include: [ db.tags ]
    }).then((news) => {
        res.json(news.dataValues);
    });
}

function getNews(req, res) {
    return db.news.findAll({
        include: [
            db.tags,
            db.users
        ]
    }).then((news) => {
        res.json(news);
    });
}

function getUserNews(req, res) {
    return db.news.findAll({
        where: {
            userId: req.params.id
        },
        include: [
            db.tags,
            db.users
        ]
    }).then((news) => {
        res.json(news);
    });
}

function getNewsByTag(req, res) {
    return db.news.findAll({
        include: [
            {
                model: db.tags,
                where: {
                    text: req.params.tag
                }
            },
            db.users
        ]
    }).then((news) => {
        res.json(news);
    });
}

function searchNews (req, res) {
    let search = req.params.search;
    return db.news.findAll({
        where: {
            $or: [
                {title: {
                    $like: `%${search}%`
                }},
                {text: {
                    $like: `%${search}%`
                }},
                db.Sequelize.literal(`tags.text LIKE '%${search}%'`)
            ]
        },
        include: [
            {
                model: db.tags,
            },
            db.users
        ]
    }).then((news) => {
        res.json(news);
    });
}
module.exports = {createNews, getNews, getUserNews, getNewsByTag, searchNews};
const db = require('../models');
const jwt = require('jsonwebtoken');
const jwtsecret = require('../config/config.json').jwtsecret;


function logout(req, res) {
    req.logout();
    res.redirect('/');
}

function getUser(req, res) {
    return db.users.find({
        where: {
            id: req.params.id
        },
    }).then((user) => {
        const payload = {
            id: user.id,
            email: user.email,
            name: user.name,
            surname: user.surname,
            description: user.description,
            image: user.image
        };
        res.json(payload);
    });
}
function updateUser(req, res) {
    let filePath = '';
    if (req.file) {
        filePath = '/' + req.file.path.substr(7);
    } else {
        filePath = req.body.perviousImage;
    }
    return db.users.update({
            surname: req.body.surname,
            name: req.body.name,
            description: req.body.description,
            image: filePath
        }, {
        where: {
            id: req.currentUser.id
        },
        returning: true,
        plain: true}).then((user) => {
        user = user[1];
        const payload = {
            id: user.id,
            email: user.email,
            name: user.name,
            surname: user.surname,
            description: user.description,
            image: user.image
        };
        const token = jwt.sign(payload, jwtsecret);
        res.json({user: payload, token: token});
    });
}



module.exports = { logout, getUser, updateUser };